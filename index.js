import express from "express";
import bodyParser from "body-parser";
import bcrypt from "bcryptjs";
import cors from "cors";
import dotenv from "dotenv";

dotenv.config();

const app = express();

app.use(cors()); // Use this after the variable declaration
import mysql from "mysql";
import { emailRouter } from "./email.restRouter";

const JWT_SECRET = "password";
// parse application/json
app.use(bodyParser.json());

/*
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
*/

//create database connection
const conn = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "restful_db",
});
//connect to database
conn.connect((err) => {
  if (err) throw err;
  console.log("Mysql Connected...");
});

app.post("/auth/login", (req, res) => {
  let data = { email: req.body.email, password: req.body.password };
  let user;
  let sql =
    "SELECT name, email, password FROM users WHERE email='" + data.email + "'";
  let query2 = conn.query(sql, (err, results) => {
    if (err) res.status(500).send("Error on the server.");
    user = results[0];
    if (!user) return res.status(404).send("No user found.");
    let passwordIsValid = bcrypt.compareSync(data.password, user.password);
    if (!passwordIsValid) {
      return res.status(401).send("Password is incorrect!!!");
    } else {
      res.send({ message: "Credentials shared are valid!!!", name: user.name });
    }
  });
});

//create user
app.post("/api/user/register", (req, res) => {
  let data = {
    name: req.body.name,
    email: req.body.email,
    password: "",
  };

  let password = "";
  console.log(req.body);

  let select = `select count(email) as count from users where email='${data.email}'`;
  let query1 = conn.query(select, (err, results) => {
    if (err) res.status(500);
    let [row] = results;

    if (row.count === 1) {
      res.status(403).send("Email id already exist");
    } else {
      password = req.body.password;
      bcrypt.genSalt(10, function (err, salt) {
        if (err) throw err;
        bcrypt.hash(password, salt, function (err, hash) {
          if (err) throw err;
          data.password = hash;
          let sql = "INSERT INTO users SET ?";
          let query2 = conn.query(sql, data, (err, results) => {
            if (err) throw err;
            res.send(
              JSON.stringify({ status: 200, error: null, response: results })
            );
          });
        });
      });
    }
  });
});

//fetch event_types
app.get("/api/event_types/:user", (req, res) => {
  let select = `select title, duration from event_types where user='${req.params.user}'`;
  let query1 = conn.query(select, (err, results) => {
    if (err) res.status(500);
    if (results) {
      res.send(results);
      //  console.log("Event types: ", data);
    }
    //console.log("Show Event Types", results);
  });
});

//create event_type
app.post("/api/event_type/create", (req, res) => {
  let data = {
    title: req.body.title,
    duration: req.body.duration,
    user: req.body.user,
  };

  console.log(req.body);

  let select = `select count(title) as count from event_types 
  where title='${data.title}' and user='${data.user}'`;
  let query1 = conn.query(select, (err, results) => {
    if (err) res.status(500);
    console.log(results);
    let [row] = results;

    if (row.count === 1) {
      res.status(403).send("Event type already exists");
    } else {
      let sql = "INSERT INTO event_types SET ?";
      let query2 = conn.query(sql, data, (err, results) => {
        if (err) res.status(500);
        if (results) {
          console.log(results);
          res.send("Event type created!!!");
        }
      });
    }
  });
});

//Edit event_type
app.put("/api/event_type/edit/:title", (req, res) => {
  console.log("Editing");
  let sql =
    "UPDATE event_types SET title='" +
    req.body.title +
    "', duration='" +
    req.body.duration +
    "' WHERE title='" +
    req.params.title +
    "' and user='" +
    req.body.user +
    "'";
  let query = conn.query(sql, (err, results) => {
    if (err) res.status(500);
    if (results) {
      console.log(results);
      res.send("Event type has been edited successfully!!!");
    }
  });
});

//Delete event_type
app.delete("/api/event_type/delete/:user/:title", (req, res) => {
  let sql =
    "DELETE FROM event_types WHERE title='" +
    req.params.title +
    "' and user='" +
    req.params.user +
    "'";
  let query = conn.query(sql, (err, results) => {
    if (err) res.status(500);
    if (results) {
      console.log(results);
      res.send("Event type has been edited successfully!!!");
    }
  });
});

app.use("/reset_password", emailRouter);
//Server listening
app.listen(3001, () => {
  console.log("Server started on port 3001...");
});
